import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AccountModule } from './account/account.module';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: '60.205.255.178',
      port: 3306,
      username: 'root',
      password: '114201mxx',
      database: 'serverless_nest',
      synchronize: true, // 自动同步数据模型到数据表中
      logging: true, // 是否打印日志
      entities: [__dirname + '/**/*.entity{.ts,.js}'],
    }),
    AccountModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
